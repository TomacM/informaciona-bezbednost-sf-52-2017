$(document).ready(function(){
	$('#regSubmit').on('click', function(event) {
		window.location.href = 'Registracija.html';
	});
	
	$('#loginSubmit').on('click', function(event) {
		event.preventDefault();
		var emailInput = $('#emailInput');
		var passwordInput = $('#passwordInput');
		
		var email = emailInput.val();
		var password = passwordInput.val();
		
		if($('#emailInput').val() == "" || $('#passwordInput').val() == ""){
            alert('Niste uneli sve potrebne informacije!');
            return;
        }
		
		$.post('korisnici/korisnik/login', {'email': email, 'password': password},
			function(response){
				if(response.email!==undefined){
				var userEmail = response.email;
				sessionStorage.setItem('userEmail', userEmail);
				console.log(response.email)
				if(response.authority.name == 'Admin'){
					window.location.href = 'admin.html';
				}else {
					window.location.href = 'user.html';
				}
				}else{
					alert("Molimo registrujte se!")
				}
		}).fail(function(){
			console.log("error")
		
		});
	});
});