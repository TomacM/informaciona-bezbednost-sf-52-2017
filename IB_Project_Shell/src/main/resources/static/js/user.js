$(document).ready(function(){

	if(sessionStorage.getItem('userEmail') == "") {
		window.location.replace('index.html');
	}
	var inputEmail = $('#inputEmail');
    var inputActive = $('#inputActive');
    var inputAuthority = $('#inputAuthority');
    var userEmail = sessionStorage.getItem('userEmail');
    currentUserInfo(userEmail);
    function currentUserInfo(email) {
    	$.get('korisnici/korisnik/email', {'email': email},
    		function(response){
				console.log(response);
				inputEmail.val(response.email).trigger("change");
				if(response.active) {
					inputActive.val("Aktivan").trigger("change");
					if(response.certificate != "") {
						$("#buttonJks").hide();
					}
				}else {
					inputActive.val("Neaktivan").trigger("change");
					$("#buttonJks").hide();
				}
				inputAuthority.val(response.authority.name).trigger("change");
		}).fail(function(){
			console.log("error");
		});
    }
    $("#buttonJks").click(function (e) {
    	var email = sessionStorage.getItem('userEmail');
    	$.get('korisnici/korisnik/jks', {'email': email},
    		function(){
    		$("#buttonJks").hide();
	    	alert("Uspesno napravljena JKS datoteka");
		}).fail(function(){
			console.log("error");
		});
    	e.preventDefault();
    });
    
    $('#btnLogout').click(function (){
    	sessionStorage.setItem('userEmail', "");
    });
    
});