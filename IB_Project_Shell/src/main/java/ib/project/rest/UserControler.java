package ib.project.rest;

import java.security.KeyPair;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ib.project.certificate.CertificateGenerator;
import ib.project.keystore.KeyStoreWritter;
import ib.project.model.Authority;
import ib.project.model.User;
import ib.project.service.AuthorityService;
import ib.project.service.UserService;

@RestController
@RequestMapping(value = "korisnici")
public class UserControler {
	private String PASS = "123";

	@Autowired
	public AuthorityService authorityService;
	@Autowired
	public UserService userService;
	private static KeyStoreWritter keyStoreWriter = new KeyStoreWritter();
	private static CertificateGenerator newCertificate = new CertificateGenerator();

	// Putanja koja pronalazi sve korisnike
	@GetMapping(path = "/")
	public ArrayList<User> findAll() {
		return userService.findAll();
	}

	@GetMapping(path = "korisnik/email")
	public ResponseEntity<User> userEmail(@RequestParam String email) {

		User user = userService.findByEmail(email);
		if (user != null) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} else {
			System.out.println("User with given email doesn't exist!");

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(path = "korisnik/jks")
	public ResponseEntity<String> userJks(@RequestParam String email) {

		User user = userService.findByEmail(email);
		if (user != null) {
			char[] password = PASS.toCharArray();

			String keyStoreFile = locationKeyStore(email);

			KeyStore keyStore = keyStoreWriter.loadKeyStore(null, password);

			keyStoreWriter.saveKeyStore(keyStore, keyStoreFile, password);

			KeyPair keyPair = newCertificate.generateKeyPair();

			keyStore = keyStoreWriter.loadKeyStore(keyStoreFile, password);

			X509Certificate certificate = newCertificate.generateCertificate(keyPair, email);

			keyStoreWriter.addToKeyStore(keyStore, email, keyPair.getPrivate(), password, certificate);

			keyStoreWriter.saveKeyStore(keyStore, keyStoreFile, password);

			user.setCertificate(keyStoreFile);
			userService.save(user);
			newCertificate.printCertificate(certificate);
			System.out.println("Napravljen JKS fajl");

			return new ResponseEntity<String>("", HttpStatus.OK);
		} else {
			System.out.println("Nema korisnika sa tim mejlom!");

			return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
		}
	}

	private String locationKeyStore(String email) {
		String path = "C:\\Users\\tomac\\git\\brezBednost\\Mail Client V2\\data\\" + email + ".jks";
		String keyStoreFile = path;
		return keyStoreFile;
	}

	@PostMapping(path = "korisnik/login")
	public ResponseEntity<User> loginUser(@RequestParam String email, @RequestParam String password) {
		String message = "Nema korisnika sa tim mejlom!";

		User user = userService.findByEmailAndPassword(email, password);
		try {

			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {

			System.out.println(message);

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(path = "korisnik/activate")
	public ResponseEntity<String> activateUser(@RequestParam String email) {

		userService.activateUser(email);

		return new ResponseEntity<String>(email, HttpStatus.OK);
	}

	@PostMapping(path = "korisnik/registration")
	public ResponseEntity<User> registrationUser(@RequestParam String email, @RequestParam String password) {

		String message = "Mejl vec postoji u bazi!";
		Authority auth = authorityService.findByName("Regular");
		User user = new User();
		User checkUser = userService.findByEmail(email);
		if (checkUser == null) {
			user.setActive(false);
			user.setAuthority(auth);
			user.setCertificate("");
			user.setEmail(email);
			user.setPassword(password);
			userService.save(user);

			return new ResponseEntity<User>(user, HttpStatus.CREATED);
		} else {
			System.out.println(message);
			
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}

}