package ib.project.certificate;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import ib.project.model.IssuerData;
import ib.project.model.SubjectData;

//Klasa za generisanje sertifikata
//Self-signed (vlasnik sam sebi potpisuje sertifikat)
public class CertificateGenerator {
	private String SHA256 = "SHA256WithRSAEncryption";
	private String BC = "BC";
	private String RSA = "RSA";
	private String START_DATE_VALIDATE = "2020-09-06";
	private String END_DATE_VALIDATE = "2020-09-06";
	private String FULLNAME = "Milos Tomac";
	private String SURNAME = "Tomac";
	private String GIVENAME = "Milos";
	private String FACULTY = "FTN";
	private String SUBJECT = "Informaciona bezbednost";
	private String COUNTRY = "RS";
	private String UID = "12345";
	private String ONE_STRING = "1";

	// Dodaje se provajder
	// Staticki blok sluzi nam za staticku inicijalizaciju klase
	// Kod se izvrsava samo jednom pri kompajliranju
	static {
		addingBouncyCastleProvider();
	}

	private static void addingBouncyCastleProvider() {
		Security.addProvider(new BouncyCastleProvider());
	}

	// Metoda za generisanje sertifikata
	// X509 vrsta sertifikata
	// issuerData - izdavaoc sertifikata
	// subjectData - fizicko ili pravno lice kome se izdaje sertifikat
	public X509Certificate generateCertificate(IssuerData issuerdata, SubjectData subjectdata) {
		try {
			// Klasa za generisanje sertifikata ne sadrzi privatni kljuc
			// Kreira se bilder za objekat koji sadrzi privatni kljuc
			// I sluzice za potpisivanje sertifikata
			// U ovom slucaju koristimo SHA256 algoritam za potpisivanje
			JcaContentSignerBuilder builder = new JcaContentSignerBuilder(SHA256);
			// Postavljanje provajdera - BouncyCastleProvider
			builder = builder.setProvider(BC);
			// Objekat koji pomocu bildera uzima privatni kljuc
			// Privatni kljuc od izdavaca sertifikata
			ContentSigner cSigner = builder.build(issuerdata.getPrivateKey());
			// Implementacija Sertifikata tj postavljanje podataka za generisanje
			// sertifikata
			X509v3CertificateBuilder x509v3certtificateBuilder = x509v3CertificateImplementation(subjectdata);
			// contentSigner - sadrzi privatni kljuc izdavaca sertifikata kojim se potpisuje
			// sertifikat
			// generisanje sertifikata
			X509CertificateHolder x509certificateHolder = x509v3certtificateBuilder.build(cSigner);
			// konvertovanje sertifikata u x509 sertifikat
			JcaX509CertificateConverter jcaX509certificateConverter = new JcaX509CertificateConverter();
			// Postavljanje provajdera BC- BouncyCastleProvider
			jcaX509certificateConverter = jcaX509certificateConverter.setProvider(BC);
			// konvertuje objekat u X509 sertifikat i vraca ga kao pobvratnu vrednost metode
			return jcaX509certificateConverter.getCertificate(x509certificateHolder);
		} catch (IllegalStateException | OperatorCreationException | IllegalArgumentException
				| CertificateException e) {
			System.out.println(e);
		}
		return null;
	}

	private X509v3CertificateBuilder x509v3CertificateImplementation(SubjectData subjectdata) {
		X509v3CertificateBuilder certtificateBuilder = new JcaX509v3CertificateBuilder(subjectdata.getX500name(),
				new BigInteger(subjectdata.getSerialNumber()), subjectdata.getStartDate(), subjectdata.getEndDate(),
				subjectdata.getX500name(), subjectdata.getPublicKey());
		return certtificateBuilder;
	}

	// Metoda za generisanje para kljuceva
	// Vraca par kljuceva
	public KeyPair generateKeyPair() {
		try {
			// Generisanje para kljuceva sa algoritmom RSA
			KeyPairGenerator generatorKey = KeyPairGenerator.getInstance(RSA);

			// 1024 bitni kljuc
			generatorKey.initialize(1024);

			// generisanje para kljuceva
			KeyPair keyPair = generatorKey.generateKeyPair();

			// vraca par kljuceva
			return keyPair;
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
		}
		return null;
	}

	// Metoda za generisanje sertifikata
	public X509Certificate generateCertificate(KeyPair keyPair, String mail) {

		// Datum vazenja
		SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = new Date();
		Date endDate = new Date();
		try {
			startDate = iso8601Formater.parse(START_DATE_VALIDATE);
			endDate = iso8601Formater.parse(END_DATE_VALIDATE);
		} catch (ParseException e) {
			System.out.println(e);
		}

		// Izdavaoc sertifiata
		X500NameBuilder x500Namebuilder = new X500NameBuilder(BCStyle.INSTANCE);
		builder_addRDN(mail, x500Namebuilder);
		// Serijski broj sertifikata
		String serial = ONE_STRING;

		X500Name x500Name = x500Namebuilder.build();

		// podaci za izdavaca
		IssuerData issuerdata = new IssuerData(keyPair.getPrivate(), x500Name);

		// podaci za vlasnika
		SubjectData subjectdata = new SubjectData(keyPair.getPublic(), x500Name, serial, startDate, endDate);

		// kreiranje sertifikata
		return generateCertificate(issuerdata, subjectdata);
	}

	private void builder_addRDN(String email, X500NameBuilder builder) {
		builder.addRDN(BCStyle.CN, FULLNAME);
		builder.addRDN(BCStyle.SURNAME, SURNAME);
		builder.addRDN(BCStyle.GIVENNAME, GIVENAME);
		builder.addRDN(BCStyle.O, FACULTY);
		builder.addRDN(BCStyle.OU, SUBJECT);
		builder.addRDN(BCStyle.C, COUNTRY);
		builder.addRDN(BCStyle.E, email);
		builder.addRDN(BCStyle.UID, UID);
	}

	public void printCertificate(X509Certificate certificate) {
		print(certificate);
	}

	private void print(X509Certificate certificate) {
		System.out.println("ISSUER: " + certificate.getIssuerX500Principal().getName());
		System.out.println("SUBJECT: " + certificate.getSubjectX500Principal().getName());
		System.out.println("Sertifikat:");
		System.out.println(
				"--------------------------------------------------------------------------------------------------------------");
		System.out.println(certificate);
		System.out.println(
				"--------------------------------------------------------------------------------------------------------------");
		System.out.println(
				"--------------------------------------------------------------------------------------------------------------");
	}
}