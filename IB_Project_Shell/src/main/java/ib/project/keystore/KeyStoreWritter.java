package ib.project.keystore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.Certificate;

public class KeyStoreWritter {
	private String JKS = "JKS";

	public KeyStore loadKeyStore(String keyStoreFilePath, char[] password) {
		KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance(JKS);
			if (keyStoreFilePath != null) {
				keyStore.load(new FileInputStream(keyStoreFilePath), password);
			} else {
				keyStore.load(null, password);
			}
		} catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
			e.printStackTrace();
			printError();
		}
		return keyStore;
	}

	private void printError() {
		System.err.println("\nGreska prilikom ucitavanja KeyStora!\n");
	}
	
	public void saveKeyStore(KeyStore keyStore, String keyStoreFilePath, char[] password) {
		try {
			keyStore.store(new FileOutputStream(keyStoreFilePath), password);
		} catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
			printErrorSavePrivateKey(e);
		}
	}

	private void printErrorSavePrivateKey(Exception e) {
		e.printStackTrace();
		System.err.println("\nGreska prilikom snimanja KeyStore!\n");
	}
	public void addToKeyStore(KeyStore keyStore, String alias, PrivateKey privateKey, char[] password,
			Certificate certificate) {
		try {
			keyStore.setKeyEntry(alias, privateKey, password, new Certificate[] { certificate });
		} catch (KeyStoreException e) {
			printErrorSaveKeyStore(e);
		}
	}

	private void printErrorSaveKeyStore(KeyStoreException e) {
		e.printStackTrace();
		System.err.println(
				"\nGreska prilikom snimanja \n");
	}

}
