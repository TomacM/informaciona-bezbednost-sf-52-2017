package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Document;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import signature.VerifySignatureEveloped;
import support.MailHelper;
import support.MailReader;
import xml.AsymmetricDecryption;
import xml.CreateXMLDOM;

public class ReadMailClient extends MailClient {

	public static boolean ONLY_FIRST_PAGE = true;
	public static long PAGE_SIZE = 3;
	private static String EMPTY="";
	private static String ME="me";
	private static String QUERY="is:unread label:INBOX";
	private static String FROM="From";
	private static String TO ="To";
	
	static {
		//Definisanje provajdera
		//Staticka inicijalizacija izvrsava se pri samom kompajliranju
		Security.addProvider(new BouncyCastleProvider());
		org.apache.xml.security.Init.init();
	}


	public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException, MessagingException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, UnrecoverableKeyException, KeyStoreException {
		// Build a new authorized API client service.
		Gmail service = getGmailService();
		ArrayList<MimeMessage> mimeMessages = new ArrayList<MimeMessage>();

		String sender = EMPTY;
		String reciever = EMPTY;

		String user = ME;
		String query = QUERY;

		List<Message> messages = MailReader.listMessagesMatchingQuery(service, user, query, PAGE_SIZE, ONLY_FIRST_PAGE);
		for (int i = 0; i < messages.size(); i++) {
			Message fullM = MailReader.getMessage(service, user, messages.get(i).getId());

			MimeMessage mimeMessage;
			try {
				mimeMessage = MailReader.getMimeMessage(service, user, fullM.getId());
				sender = mimeMessage.getHeader(FROM, null);
				reciever = mimeMessage.getHeader(TO, null);
				printMessage(sender, i, mimeMessage);
				mimeMessages.add(mimeMessage);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Izabrati poruku za dekriptovanje:");
		Integer answer = indexMessage();
		MimeMessage chosenMessage = mimeMessages.get(answer);
		// Preuzimanje attachment-a iz mail-a
		MailHelper.getAttachment(chosenMessage);
		// Dekriptovanje
		AsymmetricDecryption.testIt(sender, reciever);
		// Verifikovanje digitalnog potpisa
		VerifySignatureEveloped.testIt(sender);
		// Ispis mail-a
		ispisMejla(sender);
		Document doc = AsymmetricDecryption.loadDocument("./data/" + sender + "_dec.xml");
		CreateXMLDOM.printEmail(doc);
		//KONTROLNA TACKA
		//-----------------------------------------------------------
//		// Pomocu mail bodija izvlacimo deo sa enkriptovanom porukkom
//		MailBody mb = new MailBody(MailHelper.getText(chosenMessage));
//		byte[] secretKeyBytes = null;
//		KeyStore ks = getKeyStore();
//		String alias = "userb";
//		String password = "milosnike";
//		PrivateKey key = (PrivateKey) ks.getKey(alias, password.toCharArray());
//		Cipher rsaCipherDec = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//		rsaCipherDec.init(Cipher.DECRYPT_MODE, key);
//
//		secretKeyBytes = rsaCipherDec.doFinal(mb.getEncKeyBytes());
//		for (byte b : secretKeyBytes)
//			System.out.print(b);
//		System.out.println("\n");
//
//		Cipher aesCipherDec = Cipher.getInstance("AES/CBC/PKCS5Padding");
//		SecretKey secretKey = new SecretKeySpec(JavaUtils.getBytesFromFile(KEY_FILE), "AES");
//
//		byte[] iv1 = JavaUtils.getBytesFromFile(IV1_FILE);
//		IvParameterSpec ivParameterSpec1 = new IvParameterSpec(iv1);
//		aesCipherDec.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec1);
//
//		System.out.println(mb.getEncMessage());
//
//		byte[] decyphMsg = null;
//		try {
//			decyphMsg = aesCipherDec.doFinal(mb.getEncMessageBytes());
//			String receivedBodyTxt = new String(decyphMsg);
//			System.out.println(receivedBodyTxt);
//			String decompressedBodyText = GzipUtil.decompress(Base64.decode(receivedBodyTxt));
//			System.out.println("Body text: " + decompressedBodyText);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		byte[] iv2 = JavaUtils.getBytesFromFile(IV2_FILE);
//		IvParameterSpec ivParameterSpec2 = new IvParameterSpec(iv2);
//		// inicijalizacija za dekriptovanje
//		aesCipherDec.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec2);
//
//		// dekompresovanje i dekriptovanje subject-a
//		String decryptedSubjectTxt = new String(aesCipherDec.doFinal(Base64.decode(chosenMessage.getSubject())));
//		String decompressedSubjectTxt = GzipUtil.decompress(Base64.decode(decryptedSubjectTxt));
//		System.out.println("Subject text: " + new String(decompressedSubjectTxt));
//	}
//
//	private static KeyStore getKeyStore() {
//		try {
//
//			FileInputStream is = new FileInputStream(
//					"C:\\Users\\I-Karen\\git\\ibProjekat\\Mail Client V2\\data\\userB.jks");
//			KeyStore keystore = KeyStore.getInstance("JKS", "SUN");
//			String password = "milosnike";
//			char[] passwd = password.toCharArray();
//			keystore.load(is, passwd);
//			String alias = "userb";
//
//			// Get certificate of public key
//			Certificate cert = keystore.getCertificate(alias);
//			// Get public key
//			PublicKey publicKey = cert.getPublicKey();
//
//			String publicKeyString = Base64.encodeToString(publicKey.getEncoded());
//			System.out.println(publicKeyString);
//
//			return keystore;
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}

	}


	private static void ispisMejla(String sender) {
		System.out.println("<--- EMAIL --->");
		System.out.println("From: " + sender);
	}


	private static Integer indexMessage() throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String answerStr = reader.readLine();
		Integer answer = Integer.parseInt(answerStr);
		return answer;
	}


	private static void printMessage(String sender, int i, MimeMessage mimeMessage)
			throws MessagingException, IOException {
		System.out.println("\n Message number " + i);
		System.out.println("From: " + sender);
		System.out.println("Subject: " + mimeMessage.getSubject());
		System.out.println("Body: " + MailHelper.getText(mimeMessage));
		System.out.println("\n");
	}
}
