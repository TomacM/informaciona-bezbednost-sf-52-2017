package signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;


//Potpisivanje dokumenta
public class SignEveloped {
	//Definisanje provajdera staticka inicijalizacija kod koji se izvrsava prvi pri kompajliranju aplikacije
	static {
		Security.addProvider(new BouncyCastleProvider());
		org.apache.xml.security.Init.init();
	}

	public static void testIt(String emailSender) {
		String fileIN = "./data/" + emailSender + ".xml";
		String fileOUT = "./data/" + emailSender + "_signed.xml";
		//Ucitavanje dokumenta
		Document document = loadDocument(fileIN);
		// Privatni kljuc sa kojim se potpisuje dokument
		PrivateKey pk = readPrivateKey(emailSender);
		// Ucitavanje sertifikata
		Certificate cert = readCertificate(emailSender);
		// Potipisivanje
		System.out.println("Potpisivanje...");
		document = signDocument(document, pk, cert);
		// Cuva se dokument
		saveDocument(document, fileOUT);
		System.out.println("Potipisivanje zavrseno");
	}

	private static Document loadDocument(String file) {
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new File(file));

			return document;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//Cuvanje dokumenta u xml fajl
	private static void saveDocument(Document doc, String fileName) {
		
		try {
			File outFile = new File(fileName);
			FileOutputStream f = new FileOutputStream(outFile);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);

			transformer.transform(source, result);

			f.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//Citanje sertifikata
	private static Certificate readCertificate(String email) {
		try {
			String keyStoreFile = "./data/" + email + ".jks";

			// kreiramo instancu KeyStore
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");

			// ucitavamo podatke
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStoreFile));
			ks.load(in, "123".toCharArray());

			if (ks.isKeyEntry(email)) {
				Certificate cert = ks.getCertificate(email);
				return cert;

			} else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//Citanje privatnog kljuca
	private static PrivateKey readPrivateKey(String email) {
		try {
			String keyStoreFile = "./data/" + email + ".jks";

			// kreiramo instancu KeyStore
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");

			// ucitavamo podatke
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStoreFile));
			ks.load(in, "123".toCharArray());

			if (ks.isKeyEntry(email)) {
				PrivateKey pk = (PrivateKey) ks.getKey(email, "123".toCharArray());
				return pk;
			} else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//Potpisivanje dokumenta
	private static Document signDocument(Document doc, PrivateKey privateKey, Certificate cert) {

		try {
			Element rootEl = doc.getDocumentElement();

			// kreira se signature objekat
			XMLSignature sig = new XMLSignature(doc, null, XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA1);

			// kreiraju se transformacije nad dokumentom
			Transforms transforms = new Transforms(doc);

			// iz potpisa uklanja Signature element
			// Ovo je potrebno za enveloped tip po specifikaciji
			transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);

			// normalizacija
			transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);

			// potpisuje se citav dokument (URI "")
			sig.addDocument("", transforms, Constants.ALGO_ID_DIGEST_SHA1);

			// U KeyInfo se postavalja Javni kljuc samostalno i citav sertifikat
			sig.addKeyInfo(cert.getPublicKey());
			sig.addKeyInfo((X509Certificate) cert);

			// poptis je child root elementa
			rootEl.appendChild(sig.getElement());

			// potpisivanje
			sig.sign(privateKey);

			return doc;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
