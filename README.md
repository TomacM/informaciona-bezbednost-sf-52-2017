- Podesavanje aplikacije :
    - Za pocetak izbildovati maven aplikaciju da bi se sve potrebne biblioteke ucitale, takodje kreirati bazu podataka koja se nalazi u import.sql.
    - Izmeniti putanje 
      - ib.project.rest klasa UserController linija 57 keyStoreFile.
      - u UserController-u u metodi userJKS podesiti putanju na data koji se nalazi u konzolnoj aplikaciji linija 57 linija.
      - u user
- Koriscenje aplikacije :
    - Web aplikacija:
        - Sadrzi:
            - Log in korisnika
            - Kreiranje/registraciju novih korisnika gde korisnik dobija default-nu aktivnost (neaktivan) i autority(regular).
            - Regularan korisnik ima za funkcionalnosti pregled profila, preuzimanje svog JKS fajl-a samo jednom!
            - Admin korisnik ima za funkcionalnosti pregled profila, tabelu svih korisnika, pretragu korisnika kao i pregled drugih naloga kao
              i najvaznije aktivaciju tek registrovanih(neaktivnih) korisnika. Postoji samo jedan fixni akaunt sa admin autority admin@mail.com.
        -Koriscenje:
            - Pokrenuti web aplikaciju tako sto cete startovati DemoApplication.
            - Zatim u pretrazivacu otici na https://localhost:8440
            - Registrovcete se tako sto cete kliknuti na dugme Registration odvesce vas na formu za registraciju (postojeci mejl).
            - Posto ste se upravo registrovali dobili ste za active (neaktivan) admin mora da aktivira vas nalog posto JKS fajl mogu samo da preuzmu 
              aktivni korisnici.
            - Ulogovati se kao admin email: admin@mail.com sifra: admin kliknuti na dugme korisnici i odvesce vas na stranicu sa tabelom svih korisnika
              kliknuti na mejl korisnika koga zelite da aktivirate zatim ga aktivirajte :D
            -Ulogovati se na registrovanog korisnika i tu mozete pregledati profil i skinuti jks fajl koji se cuva u Desktop aplikaciji u data
             folderu.
    - Desktop aplikacija:
        - Sadrzi:
            - Slanje sifrovane poruke kao i citanje sifrovane poruke.
            - Pokrenuti WriteMailClient.
            - Ulogovati se sa emailom , email mora biti onaj za koji ste se registrovali kao i preuzeli JKS od tog mejla koji je sacuvan u data folderu
              pod nazivom email.jks sto prestavlja Java Key Storage za tog korisnika.
            - Zatim uneti email kome zelite da posaljete taj mejl takodje mora biti preuzet jks i smesten (automatski) u data folder.
            - Uneti naslov i sadrzaj poruke pri slanju u data folderu se kreiraju tri nova xml fajla 
            - Pokrenuti ReadMailClient.
            - Ulogovati se sa emailom kome smo poslali poruku. Odabrati zeljeni email, kucanjem njegovog indeksa.
            - ReadMailClient skida i dekriptuje izabrani email, verifikuje digitalni potpis. 
            - Na kraju ce ispisati email.
          


